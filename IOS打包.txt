Cordova打包iOS程序
1. 系统和软件要求
操作系统: macOS（必需，因为iOS编译只能在Mac上进行）
Xcode: 最新版本（从Mac App Store免费下载）
Cordova CLI: 通过npm安装最新版Cordova
2. 安装Cordova CLI
打开终端
安装Cordova：npm install -g cordova
3. 创建Cordova项目
创建新项目：cordova create myApp
进入项目目录：cd myApp
4. 添加iOS平台
在项目中添加iOS平台：cordova platform add ios
5. 添加所需插件
根据需要添加插件，cordova plugin add cordova-plugin-camera
6. 构建iOS应用
把应用的WWW替换根目录下的WWW
构建应用：cordova build ios
这将在项目的platforms/ios目录下创建一个Xcode项目

7.修改config里的AllowInlineMediaplayback的值改为true

8.打开platforms/ios目录下Xcode项目。 配置账户以后进行打包即可